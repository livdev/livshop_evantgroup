@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('apanel.page.edit') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="post" action="{{ route('apanel.page.update') }}" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="idslug">{{ __('apanel.table.slug') }}</label>
                                    <input type="text" class="form-control"
                                           name="slug"
                                           value="{{$data->slug}}"
                                           id="idslug" >
                                </div>
                                <div class="form-group">
                                    <label for="idName">{{ __('apanel.table.name') }}</label>
                                    <input type="text" class="form-control"
                                           name="name"
                                           value="{{$data->name}}"
                                           id="idName" >
                                </div>
                                <div class="form-group">
                                    <label for="idTextHmtl">{{ __('apanel.table.about') }}</label>
                                    <textarea class="form-control editor_ck5"
                                              name="html_value"
                                              id="idTextHmtl" rows="20">{!! $data->html_value  !!}</textarea>
                                </div>
                                <input type="hidden" name="id" value="{{$data->id}}">
                                @csrf
                                <input type="submit" value="Обновить">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
