@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('apanel.infoblock.create') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="post" action="{{ route('apanel.oblojka.store') }}" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="idslug">{{ __('apanel.table.name') }}</label>
                                    <input type="text" class="form-control"
                                           name="name"
                                           id="idslug" >
                                </div>
                                <div class="form-group">
                                    <label for="idFile">{{ __('apanel.table.image') }}</label>
                                    <input type="file" class="form-control-file" name="namefile" id="idFile">
                                </div>


                                <div class="form-group">
                                    <label for="idTextHmtl">{{ __('apanel.table.about') }}</label>
                                    <textarea class="form-control editor_ck5"
                                              name="about"
                                              id="idTextHmtl" rows="3"></textarea>
                                </div>

                                @csrf
                                <input type="submit">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
