@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('apanel.galary.index')}}">
                        {{ __('apanel.gallary.index') }} </a>
                        / {{$dataSelectGallary->name}} </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('errors'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('errors') }}
                            </div>
                        @endif

                        <a class="btn btn-outline-success" href="{{route('apanel.galary.create')}}" role="button">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Добавить категорию
                        </a>
                        <a class="btn btn-outline-success" href="{{route('apanel.galary.createitem')}}" role="button">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Добавить фото
                        </a>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('apanel.table.id') }}</th>
                                <th scope="col">{{ __('apanel.table.name') }}</th>
                                <th scope="col">{{ __('apanel.table.image') }}</th>
                                <th scope="col">{{ __('apanel.table.event') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datas as $data)
                                <tr>
                                    <td>
                                        {{ $data->id }}
                                    </td>
                                    <td>
                                        {{ $data->name }}
                                        <br>
                                        {{ route('front.showgalaryitemone',['id'=>$data->id]) }}
                                    </td>
                                    <td>
                                        <img
                                            style="height: 200px"
                                            src=" {{ asset('gall_item/'.$data->file) }}">
                                    </td>
                                    <td>
                                        <a class="btn btn-warning"
                                           href="{{ route('apanel.galary.itemsedit',['id'=>$data->id]) }}" role="button">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-danger"
                                           href="{{ route('apanel.galary.itemdelete',['id'=>$data->id]) }}" role="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


