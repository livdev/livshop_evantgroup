@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('apanel.galary.itemsupdate') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="post" action="{{ route('apanel.galary.itemsupdate') }}" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="idslug">{{ __('apanel.table.name') }}</label>
                                    <input type="text" class="form-control"
                                           name="name"
                                           value="{{$data->name }}"
                                           id="idslug" >
                                </div>
                                <div class="form-group">
                                    <label for="idslug">Категория</label>
                                    <select class="form-control"
                                            name="gallar_id"
                                            id="idRole">
                                        @foreach($dataGall as $gall)
                                        <option value="{{$gall->id }}"
                                            @if($gall->id==$data->gallar_id)
                                                selected
                                            @endif
                                        >{{$gall->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="idFile">{{ __('apanel.table.image') }}</label>
                                        <input type="file" class="form-control-file" name="namefile" id="idFile">
                                    <img
                                        style="height: 200px"
                                        src=" {{ asset('gall_item/'.$data->file) }}">
                                </div>

                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="submit">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
