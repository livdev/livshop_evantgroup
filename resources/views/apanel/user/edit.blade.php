@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('apanel.user.create') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="post" action="{{ route('apanel.user.update') }}">

                                <div class="form-group">
                                    <label for="idName">{{ __('apanel.table.user.name') }}</label>
                                    <input type="text" class="form-control"
                                           name="name"
                                           value="{{ $user->name }}"
                                           id="idName" placeholder="Ivan I.V.">
                                </div>

                                <div class="form-group">
                                    <label for="idEmail">{{ __('apanel.table.email') }}</label>
                                    <input type="email"
                                           name="email"
                                           value="{{ $user->email }}"
                                           class="form-control" id="idEmail" placeholder="name@example.com">
                                </div>

                                <div class="form-group">
                                    <label for="idRole">{{ __('apanel.table.user.role') }}</label>
                                    <select class="form-control"
                                            name="role"
                                            id="idRole">
                                        <option value="user"
                                            @if($user->role=='user')
                                                selected
                                            @endif
                                        >{{ __('apanel.role.user') }}</option>
                                        <option value="admin"
                                            @if($user->role=='admin')
                                                selected
                                            @endif
                                        >{{ __('apanel.role.admin') }}</option>
                                    </select>
                                </div>

                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id }}">
                                <input type="submit" value="Обновить">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
