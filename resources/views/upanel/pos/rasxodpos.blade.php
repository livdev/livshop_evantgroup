
<table class="table table-striped">
    <tbody>
        <tr>
            <th>п/п</th>
            <th>
                Наименование
            </th>
            <th>
                кол-во
            </th>
            <th>
                Цена
            </th>
            <th>
                Итого
            </th>
        </tr>
    </tbody>
    <tbody>
        @foreach($dataRasbs as $dataRasb)
            <tr>
                <td>
                    {{$dataRasb->pos_ass}}
                </td>
                <td>
                    {{$dataRasb->ass->name}}
                </td>
                <td>
                    {{$dataRasb->count}}
                </td>
                <td>
                    {{$dataRasb->price}}
                </td>
                <td>
                    {{ round($dataRasb->count*$dataRasb->price,4,2) }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

