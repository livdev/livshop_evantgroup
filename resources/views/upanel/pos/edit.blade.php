@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item"><a href="{{ route('upaenl.pos') }}">Контрагенты</a></li>
                <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
            </ol>
        </nav>

        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Редактровать контрагента
                    </div>

                    <div class="card-body">
                        <form action="{{ route('upaenl.pos.update') }}" method="post">
                            <div class="form-group">
                                <label for="inputName">Название</label>
                                <input type="text" class="form-control"
                                       id="inputName"
                                       name="name"
                                       value="{{$data->name}}"
                                       placeholder="Название контрагента.">
                            </div>
                            @csrf
                            <input type="hidden" class="idrecord" name="id" value="{{$data->id}}" >
                            <button type="submit" class="btn btn-primary">Обновить</button>
                        </form>
                    </div>

                    <hr>

                    <div class="card-body">
                        <div class="form-group" style="display: flex;flex-wrap: wrap;    align-items: center;">
                            <label for="inputName">Скидка на группу</label>
                            <select class="gr_sale_pos" id="inpGrSale">
                                <option value="0">-</option>
                                @foreach($grDataList as $itemGr)
                                    <option value="{{$itemGr->id}}">{{$itemGr->name}}</option>
                                @endforeach
                            </select>
                            <input type="number" class="form-control inpSale"
                                   id="inpSale"
                                   name="sale"
                                   style="width: 200px"
                                   value="0"
                                   placeholder="%">
                            <button class="add_sale">
                                +
                            </button>
                        </div>
                        <div>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>п/п</th>
                                    <th>
                                        Наименование
                                    </th>
                                    <th>
                                        Скидка
                                    </th>
                                    <th>
                                        Действие
                                    </th>
                                </tr>
                                </tbody>
                                <tbody class="salepos_items">
                                    @foreach($saleDatas as $saleData)
                                        <tr class="rec_{{$saleData->id}}">
                                            <td>{{$saleData->id}}</td>
                                            <td>
                                                {{$saleData->group->name}}
                                            </td>
                                            <td>
                                                {{$saleData->sale}}
                                            </td>
                                            <td>
                                                <a class="delet_sale"  data-id="{{$saleData->id}}">
                                                    Удалить
                                                </a>
                                            </td>

                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>



                    <hr>

                    <div class="card-body">
                        История расходов
                        <table  class="table">
                            <thead>
                                <tr>
                                    <th>
                                        расход №
                                    </th>
                                    <th>
                                        Дата
                                    </th>
                                    <th>
                                        Сума
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataRass as $dataRas)

                                <tr>
                                    <td>
                                        {{$dataRas->id}}
                                    </td>
                                    <td>
                                        {{$dataRas->created_at}}
                                    </td>
                                    <td style="display: flex;flex-wrap: wrap;    align-items: center;">
                                        {{$dataRas->sum}}
                                        <div class="dataRas" data-record="{{$dataRas->id}}">
                                            посмотреть расход
                                        </div>
                                    </td>
                                </tr>
                                @endforeach


                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="shoRas" style="display: none">
        <div class="dataCradRas">
            <div class="title_Ras">
                <div>

                </div>
                <div>
                    <button class="closeRasShow">Закрыть</button>
                </div>
            </div>
            <div class="data_ras">
                тут расход
            </div>
        </div>
    </div>






@endsection
