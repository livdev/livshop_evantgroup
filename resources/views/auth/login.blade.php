@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container" style="display: flex;flex-wrap: wrap">


            <div class="catalog__inner news__list" style="width: 50%;padding: 5px">


                <div class="catalog__title">Войти</div>
                <div class="news__detail">
                    <form method="POST" action="{{ route('login') }}" id="form_enter_user">
                        @csrf
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Email</label>
                            </div>

                            <input type="email"
                                   class="form-control" name="email" id="en_mail" required="" placeholder="name@example.com">
                        </div>
                        <div class="form-group">
                            <div class="label">
                            <label for="en_password">Пароль</label>
                            </div>
                            <input type="password" class="form-control" name="password" id="en_password" required >
                        </div>
                        <br>
                        <button type="submit">Войти</button>
                    </form>
                </div>
                <div class="catalog__background"></div>
            </div>

            <div class="catalog__inner news__list" style="width: 50%;padding: 5px">


                <div class="catalog__title">Создать пользователя</div>
                <div class="news__detail">
                    <form method="POST" action="{{ route('reguser') }}" id="form_enter_user">
                        @csrf
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Email</label>
                            </div>

                            <input type="email"
                                   class="form-control" name="email" id="en_mail" required="" placeholder="name@example.com">
                        </div>
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Телефон</label>
                            </div>

                            <input type="text"
                                   class="form-control" name="phone" id="en_mail" required="" placeholder="777 11 222">
                        </div>
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Ф.И.О.</label>
                            </div>

                            <input type="text"
                                   class="form-control" name="name" id="en_mail" required="" placeholder="Иванов И.И.">
                        </div>
                        <div class="form-group">
                            <div class="label">
                                <label for="en_password">Пароль</label>
                            </div>
                            <input type="password" class="form-control" name="password" id="en_password" required >
                        </div>
                        <br>
                        <button type="submit">Войти</button>
                    </form>
                </div>
                <div class="catalog__background"></div>
            </div>

        </div>
    </div>
@endsection
