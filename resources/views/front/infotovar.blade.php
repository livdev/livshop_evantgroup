@extends('front.app')

@section('fbmeta')
    <meta property="og:url"                content="<?php echo url()->current();?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{$data->name}} - {{$data->price}} руб. " />

    <meta property="og:description"        content="БытМебель - Мебель на заказ г.Рыбница . {{$data->name}} - {{$data->price}} руб." />
    <meta property="og:image"              content="https://sklad.bitmebel.com/ass_tovar/{{$data->image}}" />

@endsection


@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title">
                    <h1>{{$data->name}}</h1></div>
                <div class="news__detail">
                   <div class="tovar_info">
                        <div class="tovar_info_l">

                            <img
                                style="max-width: 60%;"
                                src="https://sklad.bitmebel.com/ass_tovar/{{$data->image}}" alt="">
                            <hr>

                            <div class="numDivTovar">

                                @if($data->showoldprice==0)
                                    <div class="list__item__price">{{$data->price}} р.</div>
                                @else
                                    <div class="list__item__price">
                                        <span class="newprice" style="text-decoration: line-through;font-size: 18px">{{$data->oldprice}} р.</span>
                                        <span class="oldprire" style="font-size: 24px">{{$data->price}} р.</span>

                                    </div>
                                @endif


                                <div class="countTovar">
                                    <input type="hidden" value="1" id="count_{{$data->id}}" value="1" style="width: 66px;">
                                </div>
                            </div>


                            <div class="list__item__basket  js-open-modal addcards addcard"
                                 data-tovarid="{{$data->id}}"
                                 data-tovarname="{{$data->name}}"
                                 data-tovarprice="{{$data->price}}"
                                 data-modal="1"
                            ></div>
                        </div>
                        <div class="tovar_info_r">
                            {!!$data->html_about  !!}


                        </div>
                   </div>
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
    <div class="fixed-overlay_tovar fixed-overlay__modal">
        <div class="modal">
            <div class="modal_title">
                <div>
                    Результат
                </div>
                <div class="modal_close">
                    <button>Закрыть</button>
                </div>
            </div>
            <div class="modal_container" id="sendFormPhone">
                Товар добавлен в корзину .
                <br> <a href="{{ url('/carts') }}">В корзину</a>
            </div>
        </div>
    </div>
@endsection




@section('myjs')
    <script>

        $('.list__item__basket').click(function () {
            var  tovarid=$(this).data('tovarid');
            var  tovarname=$(this).data('tovarname');
            var  tovarprice=$(this).data('tovarprice');
            var  counttovar=$("#count_"+tovarid).val();
            $.ajax({
                url: '/api/addtovar',
                method: 'post',
                dataType: 'json',
                data: {tovarid:tovarid ,
                    tovarname:tovarname,
                    counttovar:counttovar,
                    price:tovarprice,
                    "_token": $('meta[name="csrf-token"]').attr('content'),},
                success: function(data){
                    console.log(data);
                    $('.fixed-overlay_tovar').show();
                    document.getElementById('id_list_tovars').innerHTML=data.html;
                    document.getElementById('id_sum_tovar').innerHTML=data.sum;
                }
            });
        })


        $(document).on('click', '.modal_close', function() {
            $('.fixed-overlay_tovar').hide();
        });

        $('.addPlus').click(function () {
            var  tovarid=$(this).data('tovarid');
            var  counttovar=$("#count_"+tovarid).val();
            counttovar++;
            $("#count_"+tovarid).val(counttovar);
        })

        $('.addMinus').click(function () {
            var  tovarid=$(this).data('tovarid');
            var  counttovar=$("#count_"+tovarid).val();
            if(counttovar>0)
                counttovar--;
            $("#count_"+tovarid).val(counttovar);
        })

    </script>
@endsection
