@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title">Галерея</div>
                <div class="news__detail">
                    <div class="catalog__list">
                        @foreach($datas as $data)
                            <div class="catalog__list__item"
                                 style="background: url(/gallary/{{$data->namefile}}) #ffffff center no-repeat;
                                     background-size: cover;">

                                <div class="catalog__link">
                                    <a href="{{route('front.showgalaryitem',['id'=>$data->id])}}">{{$data->name}} </a></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
@endsection
