@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title"> Заказ</div>
                <div class="news__detail">
                    <h3>Корзина пуста!</h3>
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
@endsection
