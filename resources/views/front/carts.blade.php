@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title">Оформить заявку</div>
                <div class="news__detail">
                    <div class="cart_list">
                        <div class="cart_left">
                            @foreach($valSessionCart as $item)
                                @if($item['counttovar']>0)
                                    <div class="item__carts">
                                        <div class="span_name_cart">{{$item['name']}}</div>
                                        <div class="cart_item_pric">{{$item['price']}} руб.</div>
                                        <div class="cart_item_count">
                                            Количество
                                            <input type="number"
                                                   class="coutn_cart_tovar"
                                                   id="iCountTovar{{$item['id']}}"
                                                   data-tovarid="{{$item['id']}}"
                                                   data-tovarname="{{$item['name']}}"
                                                   data-tovarprice="{{$item['price']}}"
                                                   value="{{$item['counttovar']}}">
                                        </div>
                                    </div>

                                @endif

                            @endforeach
                        </div>
                        <div class="cart_right">
                            <span id="itog" style="
                                    text-align: center;
                                    width: 100%;
                                    font-size: 34px;
                                ">Итого -{{$sum}}руб.</span>
                            <form method="post" action="{{route('front.senzaivka')}}">
                                <div class="cartName">
                                    Ф.И.О. - <input type="text" name="cartName">
                                </div>
                                <div class="phoneCart">
                                    Телефон -<input type="text" name="phoneCart">
                                </div>
                                <div class="cartEmail">
                                    email - <input type="email" name="cartEmail">
                                </div>
                                <div class="commeCart">
                                    Комментарий -
                                    <textarea name="commeCart"></textarea>
                                </div>
                                @csrf
                                <input type="submit">
                            </form>
                        </div>
                    </div>

                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
@endsection
@section('myjs')
    <script>
        $('.coutn_cart_tovar').change(function () {
            var  tovarid=$(this).data('tovarid');
            var  tovarname=$(this).data('tovarname');
            var  tovarprice=$(this).data('tovarprice');
            var  counttovar=$(this).val();
            $.ajax({
                url: '/api/updatecount',
                method: 'post',
                dataType: 'html',
                data: {tovarid:tovarid ,
                    tovarname:tovarname,
                    counttovar:counttovar,
                    price:tovarprice,
                    "_token": $('meta[name="csrf-token"]').attr('content'),},
                success: function(data){
                    console.log(data);
                }
            });

            var sum=0;
            $( ".coutn_cart_tovar" ).each(function() {
               sum=sum+$(this).val()*$(this).data('tovarprice')
            });
            document.getElementById('itog').innerHTML="Итого - "+sum+" руб.";
        });
    </script>
@endsection

