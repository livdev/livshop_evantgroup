@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title">
                    <a href="{{route('front.showgalary')}}">
                        Галерея
                    </a>
                    / {{$dataSelectGallary->name}}</div>
                <div class="news__detail">
                    <div class="catalog__list">
                        @foreach($datas as $data)
                            <div class="catalog__list__item"
                                 data-id="{{$data->id}}"
                                 data-srec="{{ asset("/gall_item/".$data->file) }}"
                                 style="background: url(/gall_item/{{$data->file}}) #ffffff center no-repeat;
                                     background-size: cover;">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
@endsection

@section('myjs')
    <script>
        $('.catalog__list__item').click(function () {
            let getIdModal = document.getElementById('overlay');
            let urlImage=$(this).data('srec');
            getIdModal.style.visibility = 'visible';
            getIdModal.style.opacity = '1';
            document.getElementById("showimgmodal").src=urlImage;
            getIdModal.style.transition = 'all 0.7s ease-out 0s';
        })
    </script>
@endsection
