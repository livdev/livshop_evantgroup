<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-PQYFDB9ZYV"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-PQYFDB9ZYV');
    </script>
    <meta http-equiv="content-type" content="text/htm" charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>БытМебель - Мебель на заказ г.Рыбница</title>
    <style type="text/css">

    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '449550386219130');
        fbq('track', 'ViewContent');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=449550386219130&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->



    @yield('fbmeta')


    <meta name="google-site-verification" content="4UUxkDKKGq7qGI_4n2xnARZdlHkX9h_V7x7i1XRvhsk" />
</head>
<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v9.0&appId=732736434000789&autoLogAppEvents=1" nonce="pFzvBGl1"></script>
<head>
    <div class="head__d">
        <div class="head__d_wrapper">
            <nav class="head__d_menu1">
                <a href="{{ url('/')}}" class="head__d_menu1_btn">ГЛАВНАЯ</a>
                <a href="{{ url('/page/ysluga')}}" class="head__d_menu1_btn">УСЛУГИ</a>
                <a href="{{ route('front.shopitems',['id'=>1])}}" class="head__d_menu1_btn">МЕБЕЛЬНАЯ ФУРНИТУРА</a>
                <a href="{{ route('front.shopitems',['id'=>2])}}" class="head__d_menu1_btn">ПЛИТНЫЕ МАТЕРИАЛЫ</a>
                <a href="{{ route('front.shopitems',['id'=>3])}}" class="head__d_menu1_btn">КРОМОЧНЫЕ МАТЕРИАЛЫ</a>
                <a href="{{ route('front.shopitems',['id'=>52])}}" class="head__d_menu1_btn">Мойки</a>
                <a href="{{ route('front.showgalary')}}" class="head__d_menu1_btn">ГАЛЕРЕЯ</a>
            </nav>
            <div class="head__d_logodiv">
                <div class="head__d_logodiv_logoimg">
                    <a href="{{ url('/')}}">
                        <img src="{{asset('img/logo.png')}}">
                    </a>
                </div>
                <div class="head__d_logodiv_logoadress">
                    Мебельное ателье <br>
                    ул. Чернышевского 6А <br>
                    www.bitmebel.com <br>
                    077749822,<br>
                    077754155<br>
                </div>
                <div class="head__d_logodiv_btn">
                    <button class="head__d_logodiv_btn_com button">Обратная связь</button>
                </div>
                <div class="head__d_logodiv_search">
                    <input type="text" name="search" placeholder="Поиск по сайту" class="head__d_logodiv_search_input">
                </div>
                <div class="head__d_user_nav">
                    <!--
                    <div class="head_d_enter">
                        @guest
                            <button class="header__enter button">Авторизация на сайте</button>
                        @else
                            <button class="header__enter button">Личный кабинет</button>
                        @endif

                    </div>
                    -->
                    <div class="head_d_cat">
                        <span id="d_count_tovar" style="position: absolute;
                                /* top: 2px; */
                                margin-top: 21px;
                                margin-left: 94px;
                                background-color: #cda413;
                                padding: 9px;
                                border-radius: 30px;
                                display: none;
                                z-index: 22;
                                color: white;
                            ">0</span>
                        <button class="header__basket button"></button>
                    </div>
                </div>
            </div>


        </div>
        <div class="header__bottom">
            <div class="header__bottom__inner">
                <div class="header__bottom__section"><a href="{{ url('/page/ysluga')}}">Услуги</a></div>
                <div class="header__bottom__section"><a  href="{{ route('front.shopitems',['id'=>1])}}">Мебельная фурнитура</a></div>
                <div class="header__bottom__section"><a href="{{ route('front.shopitems',['id'=>2])}}">Плитные материалы</a></div>
                <div class="header__bottom__section"><a href="{{ route('front.shopitems',['id'=>3])}}">Кромочные материалы</a></div>
                <div class="header__bottom__section"><a href="{{ route('front.shopitems',['id'=>52])}}">Мойки</a></div>
                <div class="header__bottom__section"><a href="{{ route('front.showgalary')}}">Галерея</a></div>
            </div>
        </div>
    </div>

    <div class="head__m">
        <div class="div_mobile">
            <div class="head__m_nav1" >
                <a href="{{ url('/')}}">
                    <img src="{{asset('img/logo.png')}}" class="head__m_logo">
                </a>
            </div>
            <div class="head__m_nav1" >
                <div class="hamburger-menu">

                        <img src="{{asset('img/cartmob1.png')}}" class="head__m_logo_cart header__basket">

                    <input id="menu__toggle" type="checkbox" />
                    <label class="menu__btn" for="menu__toggle">
                        <span></span>
                    </label>

                    <ul class="menu__box">
                        <li><a class="menu__item" href="{{ url('/')}}">Главная</a></li>
                        <li><a class="menu__item" href="{{ url('/page/ysluga')}}">Услуги</a></li>
                        <li><a class="menu__item"  href="{{ route('front.shopitems',['id'=>1])}}">Мебельная фурнитура</a></li>
                        <li><a class="menu__item" href="{{ route('front.shopitems',['id'=>2])}}">Плитные материалы</a></li>
                        <li><a class="menu__item"href="{{ route('front.shopitems',['id'=>3])}}">Кромочные материалы</a></li>
                        <li><a class="menu__item"href="{{ route('front.shopitems',['id'=>52])}}">Мойки</a></li>
                        <li><a class="menu__item" href="{{ route('front.showgalary')}}">Галерея</a></li>
                        <li><a href="#" class="menu__item">Вxод</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</head>

@yield('content')


<div class="contact">
    <h2>Контакты</h2>
    <div class="info_contact">
        <div class="info_contact_blog">
            {!!   \App\Services\InfoblocService::getHtml('footer1') !!}
        </div>
        <div class="info_contact_blog">
            <b>МЫ В VIBER</b> <br>
            <a href="https://invite.viber.com/?g2=AQAeouXkaWTRekvmtO8qad%2B8R%2FymfMOPKz4SCQ3AjH0XRdXBrvn1iDlOzx7Y3qSz">
                <img src=" {{ asset('img/viber.png')}}" class="viber_img"><br>
            </a>
        </div>
        <div class="info_contact_blog">
            <b>МЫ В INSTAGRAM</b><br>
            <a href="https://www.instagram.com/bitmebelimelnik/">
                <img src=" {{ asset('img/inst.png')}}" class="viber_img"><br>
            </a>
        </div>
    </div>
</div>

<div id="overlay">
    <div class="popup">
        <button class="close" title="Закрыть окно" onclick="swa2()"></button>
        <p class="zag">Фото c галереи</p>
        <img class="modal_img"
             id="showimgmodal"
             style="width: 100%;max-height: 80vh;"
             src="img/gal/1.jpg" >
    </div>
</div>


<div class="fixed-overlay fixed-overlay__modal">
    <div class="modal">
        <div class="modal_title">
            <div>
                Обратная связь
            </div>
            <div class="modal_close">
                <button>Закрыть</button>
            </div>
        </div>
        <div class="modal_container" id="sendFormPhone">

            <input type="text" placeholder="Номер телефона" id="sendPhone"
                   class="popUp__input">
            <br>
            <button class="send_vizov">Отпавить</button>
        </div>
    </div>
    @extends('front.cartmodal')
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script>

    var b = document.getElementById('overlay');
    function swa(id){
        b.style.visibility = 'visible';
        b.style.opacity = '1';
        document.getElementById("showimgmodal").src="/oblojka/"+id;
        b.style.transition = 'all 0.7s ease-out 0s';
    }
    function swa2(){
        b.style.visibility = 'hidden';
        b.style.opacity = '0';
    }



    $(function() {
        menu_top = 40;        // запоминаем положение меню
        $(window).scroll(function () {             // отслеживаем событие прокрутки страницы
            if ($(window).scrollTop() > menu_top) {  // если прокрутка дошла до меню
                if ($('.head__m').css('position') != 'fixed') {  // проверяем, если меню еще не зафиксировано
                    $('.head__m').css('position','fixed');  // задаем блоку меню свойство position = fixed
                    $('.head__m').css('top','0');           // положение в самом верху
                    $('.head__m').css('width','100%');
                    $('.content').css('margin-top','40px'); // делаем отступ, чтобы контент не "скакал" в момент фиксации меню
                }
            } else {                                 // прокрутка страницы обратно вверх достигла место "перехода" меню
                if ($('.head__m').css('position') == 'fixed') {  // если меню зафиксировано
                    $('.head__m').css('position','');
                    $('.head__m').css('top','');
                    $('.content').css('margin-top','');
                }
            }
        });


        $.ajax({
            url: '/api/countovar',
            method: 'post',
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),},
            success: function(data){
                console.log(data);
                if(data>0){
                    $('#d_count_tovar').text(data);
                    $('#d_count_tovar').show();
                }
            }
        });


    });

    $( ".header__enter" ).click(function() {
        document.location.href = ('/account');
    });

    $( ".header__basket" ).click(function() {
        //document.location.href = ('/carts');
        $('.fixed-overlay_tovar').show();
        var  tovarid=$(this).data('tovarid');
        if(tovarid==undefined)
            return true
        var  tovarname=$(this).data('tovarname');
        var  tovarprice=$(this).data('tovarprice');
        var  image=$(this).data('image');
        var  counttovar=0;
        $.ajax({
            url: '/api/addtovar',
            method: 'post',
            dataType: 'json',
            data: {tovarid:tovarid ,
                tovarname:tovarname,
                image:image,
                counttovar:counttovar,
                price:tovarprice,
                "_token": $('meta[name="csrf-token"]').attr('content'),},
            success: function(data){
                console.log(data.html);
                document.getElementById('id_list_tovars').innerHTML=data.html;
                document.getElementById('id_sum_tovar').innerHTML=data.sum;
            }
        });
    });


    $(document).on('click', '.modal_close', function() {
        $('.fixed-overlay_tovar').hide();
    });


    $(document).on('click', '.head__d_logodiv_btn_com', function() {
        $('.fixed-overlay').show('animation-open');
    });
    $(document).on('click', '.head__d_logodiv_btn_com1', function() {
        $('.fixed-overlay').show('animation-open');
    });
    $(document).on('click', '.modal_close', function() {
        $('.fixed-overlay').hide('animation-close');
    });

    $(document).on('click', '.btn_cart_ajax', function() {
        let myType=$(this).data('type');
        let idTovar=$(this).data('tovarid');
        let priceTovar=$(this).data('tovarprice');
        let valNow=$('#iCountTovar'+idTovar).val();
        if(myType=="minus"){
            if(valNow==0)
                return;
            valNow=valNow-1;
            $('#iCountTovar'+idTovar).val(valNow);
            $('#iCountTovar'+idTovar).change();
        }
        if(myType=="plus"){
            valNow++;
            $('#iCountTovar'+idTovar).val(valNow);
            $('#iCountTovar'+idTovar).change();
        }
        let sumTovar=valNow*priceTovar;
        document.getElementById('itogo_'+idTovar).innerHTML=sumTovar;
        let sumItogo=0;
        $( ".price_tovar" ).each(function( index ) {
            let itogo=$(this).text();
            let nItogo=parseFloat(itogo);
            //console.log(nItogo);
            sumItogo=sumItogo+nItogo;
            //console.log( index + ": " + $( this ).text().toFixed(2));
        });
        //id_sum_tovar
        console.log(sumItogo);
        $('#id_sum_tovar').text(sumItogo)


    });


    //delete_zero
    $(document).on('click', '.delete_zero', function() {
        let idTovar=$(this).data('tovarid');
        $('#iCountTovar'+idTovar).val(0);
        $('#item_cart_tovar_'+idTovar).hide();
        $('#iCountTovar'+idTovar).change();
    });


    $(document).on('click', '.send_vizov', function() {
        if($('#sendPhone').val()==""){
            alert('Пустое поле');
            return 1;
        }
        $('.send_vizov').hide();
        $.ajax({
            type: "POST",
            url: "/sendphone",
            data: {
                "_token": "{{ csrf_token() }}",
                "text": $('#sendPhone').val(),
            },
            success: function (data) {
                document.getElementById('sendFormPhone').innerHTML="<b>Заявка отправлена , ждите звонка</b>";
            },
            error: function (data) {
            },
        });
    });

    $(document).on('change', '.coutn_cart_tovar', function() {
        var  tovarid=$(this).data('tovarid');
        var  tovarname=$(this).data('tovarname');
        var  tovarprice=$(this).data('tovarprice');
        var  counttovar=$(this).val();
        var  image=$(this).data('image');
        $.ajax({
            url: '/api/updatecount',
            method: 'post',
            dataType: 'html',
            data: {tovarid:tovarid ,
                tovarname:tovarname,
                counttovar:counttovar,
                image:image,
                price:tovarprice,
                "_token": $('meta[name="csrf-token"]').attr('content'),},
            success: function(data){
                console.log(data);
                $.ajax({
                    url: '/api/countovar',
                    method: 'post',
                    data: {
                        "_token": $('meta[name="csrf-token"]').attr('content'),},
                    success: function(data){
                        console.log(data);
                        if(data>0){
                            $('#d_count_tovar').text(data);
                            $('#d_count_tovar').show();
                        }

                    }
                });
            }
        });

        let sumTovar=counttovar*tovarprice;
        document.getElementById('itogo_'+tovarid).innerHTML=sumTovar;

        let sumItogo=0;
        $( ".price_tovar" ).each(function( index ) {
            let itogo=$(this).text();
            let nItogo=parseFloat(itogo);
            //console.log(nItogo);
            sumItogo=sumItogo+nItogo;
            //console.log( index + ": " + $( this ).text().toFixed(2));
        });
        //id_sum_tovar
        console.log(sumItogo);
        $('#id_sum_tovar').text(sumItogo)

    });





</script>

@yield('myjs')

<script src="//code.jivo.ru/widget/yyxLWULNyI" async></script>

</body>
