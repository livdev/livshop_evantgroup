@extends('front.app')

@section('fbmeta')
    <meta property="og:url"                content="<?php echo url()->current();?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="Evantroup - Мебель на заказ г.Рыбница" />
    <meta property="og:description"        content="{{$data->name}}" />
    <meta property="og:image"              content="{{ asset("/gall_item/".$data->file) }}" />
@endsection

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title">
                    <a href="{{route('front.showgalary')}}">
                        Галерея
                    </a>
                   </div>
                <div class="news__detail">
                    <div class="catalog__list">
                            <div class="catalog__list__item"
                                 data-id="{{$data->id}}"
                                 data-srec="{{ asset("/gall_item/".$data->file) }}"
                                 style="background: url(/gall_item/{{$data->file}}) #ffffff center no-repeat;
                                     background-size: cover;
                                     width: 100%;
                                     height: 500px;">
                            </div>
                    </div>
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
@endsection

@section('myjs')
    <script>
        $('.catalog__list__item').click(function () {
            let getIdModal = document.getElementById('overlay');
            let urlImage=$(this).data('srec');
            getIdModal.style.visibility = 'visible';
            getIdModal.style.opacity = '1';
            document.getElementById("showimgmodal").src=urlImage;
            getIdModal.style.transition = 'all 0.7s ease-out 0s';
        })
    </script>
@endsection
