@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title">{{$name_group}} </div>
                <div class="news__detail">
                    <div class="catalog__list_shop">
                        @foreach($datas as $data)
                            @if($data->type=="group")
                                <a href="{{ route('front.shopitems',['id'=>$data->id])}}" >
                                    <div class="catalog__list__item" style="
                                        background: url(https://sklad.bitmebel.com/groups/{{$data->image}}) center no-repeat;
                                        background-size: cover;
                                        cursor:pointer;
                                        ">
                                        <div style="
                                               background-color: #991706;
                                                position: relative;
                                                top: 198px;
                                                color: white;
                                                font-size: 16px;
                                                text-align: center;">
                                            {{$data->name}}
                                        </div>
                                    </div>
                                </a>
                            @else
                                <div class="catalog__value__item">
                                    <div class="list__item__info">
                                        <div class="list__item__img">
                                            <a href="{{ route('front.carttovar',['id'=>$data->id]) }}">
                                            <img
                                                style="max-width: 60%;"
                                                src="https://sklad.bitmebel.com/ass_tovar/{{$data->image}}" alt="">
                                            </a>
                                        </div>
                                        <div class="list__item__text">
                                            <div class="list__item__name">
                                                <a href="{{ route('front.carttovar',['id'=>$data->id]) }}">
                                                    {{$data->name}}
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="list__item__value">
                                        <div class="numDivTovar">
                                            <div class="list__item__price">{{$data->price}} р.</div>
                                            <div class="countTovar">
                                                <input type="hidden" value="1" id="count_{{$data->id}}" value="1" style="width: 66px;">
                                            </div>
                                        </div>


                                        <div class="list__item__basket  js-open-modal addcards addcard"
                                             data-tovarid="{{$data->id}}"
                                             data-tovarname="{{$data->name}}"
                                             data-tovarprice="{{$data->price}}"
                                             data-image="https://sklad.bitmebel.com/ass_tovar/{{$data->image}}"
                                             data-modal="1"
                                        ></div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>




@endsection


@section('myjs')
    <script>

        $('.list__item__basket').click(function () {
            $('.fixed-overlay_tovar').show();
            var  tovarid=$(this).data('tovarid');
            var  tovarname=$(this).data('tovarname');
            var  tovarprice=$(this).data('tovarprice');
            var  image=$(this).data('image');
            var  counttovar=$("#count_"+tovarid).val();
            $.ajax({
                url: '/api/addtovar',
                method: 'post',
                dataType: 'json',
                data: {tovarid:tovarid ,
                    tovarname:tovarname,
                    image:image,
                    counttovar:counttovar,
                    price:tovarprice,
                    "_token": $('meta[name="csrf-token"]').attr('content'),},
                success: function(data){
                    console.log(data.html);
                    document.getElementById('id_list_tovars').innerHTML=data.html;
                    document.getElementById('id_sum_tovar').innerHTML=data.sum;


                    $.ajax({
                        url: '/api/countovar',
                        method: 'post',
                        data: {
                            "_token": $('meta[name="csrf-token"]').attr('content'),},
                        success: function(data){
                            console.log(data);
                            if(data>0){
                                $('#d_count_tovar').text(data);
                                $('#d_count_tovar').show();
                            }
                        }
                    });
                }
            });
        })


        $(document).on('click', '.modal_close', function() {
            $('.fixed-overlay_tovar').hide();
        });

        $('.addPlus').click(function () {
            var  tovarid=$(this).data('tovarid');
            var  counttovar=$("#count_"+tovarid).val();
            counttovar++;
            $("#count_"+tovarid).val(counttovar);
        })

        $('.addMinus').click(function () {
            var  tovarid=$(this).data('tovarid');
            var  counttovar=$("#count_"+tovarid).val();
            if(counttovar>0)
                counttovar--;
            $("#count_"+tovarid).val(counttovar);
        })

    </script>
@endsection
