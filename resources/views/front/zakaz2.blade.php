<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Оформление заказа</title>
    <style>
        @media screen and (max-width: 768px) {
            .card-body {
                width: 100%;
            }
            .cardzakaz1{
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="background-color: #dfe1f0">

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: white">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="/img/logo.png" height="57px">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link active" aria-current="page" href="{{ url('/') }}">Главная страница</a>
            </div>
        </div>
    </div>
</nav>


<div class="row" style="padding: 10px">

    <div class="col-6 col-md-6 cardzakaz1">
        <div class="card" style="width:100%;">
            <div class="card-body">
                <form id="ajazZaivka">
                <h4>Контакты покупателя</h4>

                <div class="row">
                    <p>
                        {{$data['fio']}}
                    </p>
                    <p>
                        {{$data['phone']}}
                    </p>
                    <p>
                        {{$data['email']}}
                    </p>
                </div>
                <h4>Способ оплаты</h4>
                <div class="row">
                    @if($data['oplata']==1)
                        <p>
                            Оплата картой
                        </p>
                    @endif
                    @if($data['oplata']==2)
                        <p>
                            Наличными в офисе магазина
                        </p>
                    @endif
                        @if($data['oplata']==3)
                            <p>
                                Наложенный платеж - Почта
                            </p>
                        @endif

                </div>
                <h4>Способ доставки</h4>
                <div class="row">
                    @if($data['method_dostavki']==1)
                        <p>
                            Самовывоз
                        </p>
                    @endif
                    @if($data['method_dostavki']==2)
                            <p>
                                Доставка
                            </p>
                            <p>
                                {{$data['adress_dostavki']}}
                            </p>
                    @endif
                        @if($data['method_dostavki']==3)
                            <p>
                                Почта
                            </p>
                        @endif

                </div>

                <button
                    id="btn_submit_zakaz"
                    @guest
                    style="display: none"
                        @else
                    @endguest

                    type="submit">Оформить заказ</button>

                </form>


                <div class="row">
                    <form id="register_form" action="{{ route('registerzaivka') }}">

                        @csrf
                        <br>
                        <button type="submit">Отправить заявку</button>
                    </form>
                </div>



            </div>
        </div>
    </div>


    <input  type="hidden" id="h_name" value="{{$data['fio']}}">
    <input  type="hidden" id="h_phone" value="{{$data['phone']}}">
    <input  type="hidden" id="h_email" value="{{$data['email']}}">
    <input  type="hidden" id="h_oplata" value="{{$data['oplata']}}">
    <input  type="hidden" id="h_method_dostavki" value="{{$data['method_dostavki']}}">
    <input  type="hidden" id="h_adress_dostavki" value="{{$data['adress_dostavki']}}">

    <div class="col-6 col-md-6 cardzakaz1">
        <div class="card" style="width:100%;">
            <div class="card" style="width:100%;">
                <?php
                $datas=session()->get('cardbuy');
                $sum=0;
                foreach ($datas as $data){
                    $sum+=round( ($data['counttovar']*$data['price']),4,2) ;
                }
                ?>
                <div class="card-body">
                    <h4 class="card-title">Ваш заказ</h4>

                </div>
                @foreach($datas as $data)
                    @if($data['counttovar']>0)
                        <div class="row" style="padding: 10px">
                            <div class="col-md-3">
                                <img src="{{$data['image']}}" style="width: 100%">
                            </div>
                            <div class="col-md-9">
                                <b>{{$data['name']}}</b><br>
                                Колличество-<b>{{$data['counttovar']}}</b> / Цена-<b>{{$data['price']}} руб.</b> /  К оплате - <b> {{ round( ($data['counttovar']*$data['price']),4,2) }} руб.</b>
                            </div>

                        </div>
                        <hr>
                    @endif
                @endforeach
                    <h5>К оплате за заказ без учета доставки - {{$sum}} руб.пмр</h5>
        </div>

    </div>


</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>


    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script>
        $( "#register_form" ).submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: './registerzaivka',
                method: 'post',
                data: {
                    email:$('#reg_email').val(),
                    password:$('#reg_passowrd').val(),
                    name:$('#reg_name').val(),
                    phone:$('#reg_phone').val(),

                    h_name:$('#h_name').val(),
                    h_phone:$('#h_phone').val(),
                    h_email:$('#h_email').val(),
                    h_oplata:$('#h_oplata').val(),
                    h_method_dostavki:$('#h_method_dostavki').val(),
                    h_adress_dostavki:$('#h_adress_dostavki').val(),
                    create_order:1,


                    "_token": $('meta[name="csrf-token"]').attr('content'),},
                success: function(data){
                    console.log(data);
                    if(data=="error"){
                        alert('У вас не получилось зайти , попробуйте еще разы');
                    }
                    if(data=="login"){
                        alert('Заказ создан, ждите , Вам позвонят');
                        document.location.replace("/");
                    }
                },
                error: function (error) {
                    alert('У вас не получилось зайти , попробуйте еще разы');
                }
            });

        });

        $( "#form_enter_user" ).submit(function(event) {

            event.preventDefault();
            $.ajax({
                url: './login',
                method: 'post',
                data: {
                    email:$('#en_mail').val(),
                    password:$('#en_password').val(),
                    h_name:$('#h_name').val(),
                    h_phone:$('#h_phone').val(),
                    h_email:$('#h_email').val(),
                    h_oplata:$('#h_oplata').val(),
                    h_method_dostavki:$('#h_method_dostavki').val(),
                    h_adress_dostavki:$('#h_adress_dostavki').val(),
                    create_order:1,
                    "_token": $('meta[name="csrf-token"]').attr('content'),},
                success: function(data){
                    console.log(data);
                    if(data=="error"){
                        alert('У вас не получилось зайти , попробуйте еще разы');
                    }
                    if(data=="login"){
                        alert('Заказ создан, ждите , Вам позвонят');
                        document.location.replace("/");
                    }
                },
                error: function (error) {
                    alert('У вас не получилось зайти , попробуйте еще разы');
                }
            });
        });

        $( "#ajazZaivka" ).submit(function(event) {

            event.preventDefault();
            $.ajax({
                url: './ajxsend',
                method: 'post',
                data: {
                    h_name:$('#h_name').val(),
                    h_phone:$('#h_phone').val(),
                    h_email:$('#h_email').val(),
                    email:$('#h_email').val(),
                    h_oplata:$('#h_oplata').val(),
                    h_method_dostavki:$('#h_method_dostavki').val(),
                    h_adress_dostavki:$('#h_adress_dostavki').val(),
                    "_token": $('meta[name="csrf-token"]').attr('content'),},
                success: function(data){
                    console.log(data);
                    if(data=="error"){
                        alert('У вас не получилось зайти , попробуйте еще разы');
                    }
                    if(data=="login"){
                        alert('Заказ создан, ждите , Вам позвонят');
                        document.location.replace("/");
                    }
                },
                error: function (error) {
                    alert('У вас не получилось зайти , попробуйте еще разы');
                }
            });
        });




    </script>




</body>
</html>
