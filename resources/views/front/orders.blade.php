@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container" style="display: flex;flex-wrap: wrap">

            <div class="catalog__inner news__list" style="width: 30%;padding: 5px">
                <div class="catalog__title">Меню</div>
                <div class="news__detail">
                    <a href="{{ route('account.index') }}">Пользователь</a>
                    <br>
                    @if(Auth::user()->role=="admin")
                        <a href="/home">Админка</a>
                        <br>
                    @endif
                    <a href="{{ route('account.orders') }}">Заказы</a>
                    <br>


                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                           Выйти
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                </div>
            </div>

            <div class="catalog__inner news__list" style="width: 70%;padding: 5px">
                <div class="catalog__title">Заказы</div>
                <div class="news__detail">
                    Список заказов
                    <table border="1" width="100%">
                        <tr>
                            <td><b>Заказ номер</b></td>
                            <td><b>Дата создания</b></td>
                            <td><b>Статус заказ</b></td>
                            <td><b>Действия</b></td>
                        </tr>

                        @foreach($dataOrders as $dataOrder)
                            <?php
                                $js_column_validation = json_decode($dataOrder->data_send);
                                $js_column_validation = json_decode($js_column_validation);
                            ?>
                            <tr>
                                <td>
                                    {{ $js_column_validation->zakaz }}
                                </td>
                                <td>
                                    {{ $dataOrder->created_at }}
                                </td>
                                <td>
                                    <p id="status_{{ $js_column_validation->zakaz }}"></p>
                                </td>
                                <td>
                                    Просмотреть заказ
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="catalog__background"></div>

        </div>
    </div>
@endsection
@section('myjs')
    <script>

        $.ajax({
            url: './orders/status',
            method: 'post',
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),},
            success: function(data){

                var obj = JSON.parse(data);
                obj.forEach(function(item, i, arr) {
                    console.log(item[0].id)
                    document.getElementById('status_'+item[0].id).innerText=item[0].status;
                });

            },
            error: function (error) {
                alert('У вас не получилось, попробуйте еще разы');
            }
        });
    </script>
@endsection
