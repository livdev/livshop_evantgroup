@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container" style="display: flex;flex-wrap: wrap">

            <div class="catalog__inner news__list" style="width: 30%;padding: 5px">
                <div class="catalog__title">Меню</div>
                <div class="news__detail">
                    <a href="{{ route('account.index') }}">Пользователь</a>
                    <br>
                    @if(Auth::user()->role=="admin")
                    <a href="/home">Админка</a>
                    <br>
                    @endif
                    <a href="{{ route('account.orders') }}">Заказы</a>


                    <br>


                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                           Выйти
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                </div>
            </div>

            <div class="catalog__inner news__list" style="width: 70%;padding: 5px">
                <div class="catalog__title">Пользователь</div>
                <div class="news__detail">
                    <form  action="{{ route('registerzaivka') }}" method="post">
                        <h2>Личная информация</h2>
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Email</label>
                            </div>
                            <input type="email"
                                   value="{{ $data->email}}"
                                   style="
                                        /* border: 20px; */
                                        padding: 9px;
                                        width: 100%;
                                        font-size: 20px;
                                        background-color: antiquewhite;
                                    "
                                   readonly
                                   class="form-control"   placeholder="name@example.com">
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Ф.И.О.</label>
                            </div>
                            <input type="text"
                                   value="{{ $data->name}}"
                                   style="
                                        /* border: 20px; */
                                        padding: 9px;
                                        width: 100%;
                                        font-size: 20px;
                                    "
                                   class="form-control" name="name"  required placeholder="Иванов И.И.">
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Телефон</label>
                            </div>
                            <input type="text"
                                   name="phone"
                                   value="{{ $data->phone}}"
                                   style="
                                        /* border: 20px; */
                                        padding: 9px;
                                        width: 100%;
                                        font-size: 20px;
                                    "
                                   class="form-control"  required placeholder="Иванов И.И.">
                        </div>
                        <input type="hidden" value="1" name="type_date">
                        @csrf
                        <br>
                        <input class="form-control" type="submit" value="Обновить">
                    </form>

                    <form action="{{ route('account.update')  }}" method="post">
                        <h2>Сменить пароль</h2>

                        <div class="form-group">
                            <div class="label">
                                <label for="en_mail">Новый пароль</label>
                            </div>
                            <input type="text"

                                   style="
                                        /* border: 20px; */
                                        padding: 9px;
                                        width: 100%;
                                        font-size: 20px;
                                    "
                                   class="form-control" name="password"  required placeholder="Новый пароль">
                        </div>
                        @csrf
                        <input type="hidden" value="2" name="type_date">
                        <br>
                        <input class="form-control" type="submit" value="Обновить">
                    </form>
                </div>
            </div>
            <div class="catalog__background"></div>

        </div>
    </div>
@endsection
