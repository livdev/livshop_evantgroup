@foreach($valSessionCart as $item)
    @if($item['counttovar']>0)
    <div class="item_cart"
         id="item_cart_tovar_{{$item['id']}}"
         style="display: flex;
                                    background-color: white;
                                    padding: 5px;
                                    margin-left: 5px;
                                    justify-content: space-between;
                                    flex-wrap: wrap;">
        <div class="img_tovar"  >
            <img
                width="128"
                height="128"
                src="  {{$item['image']}}">
        </div>

        <div class="text_tovar" style="
                                        font-size: 26px;
                                        cursor: pointer;
                                        width: 50%;
                                        display: flex;
                                        flex-wrap: wrap;
                                    ">
            <div class="text_info" style="
                                        font-size: 26px;
                                        width: 100%;
                                    ">
                {{$item['name']}}
            </div>
            <div class="count_tovar_edit" style="width: 100%">
                <button
                    data-tovarid="{{$item['id']}}"
                    data-type="minus"
                    data-tovarprice="{{$item['price']}}"
                    class="btn_cart_ajax"
                    style="
                                            background: #b5bcce;
                                            width: 38px;
                                            border: none;
                                            color: white;
                                            padding: 0px 5px;
                                            text-align: center;
                                            text-decoration: none;
                                            display: inline-block;
                                            font-size: 37px;
                                        ">-</button>
                <input  class="coutn_cart_tovar"
                        id="iCountTovar{{$item['id']}}"
                        data-tovarid="{{$item['id']}}"
                        data-tovarname="{{$item['name']}}"
                        data-image="{{$item['image']}}"
                        data-tovarprice="{{$item['price']}}"
                        value="{{$item['counttovar']}}"

                        style="
                                            background: #b5bcce;
                                            width: 97px;
                                            border: none;
                                            color: white;
                                            padding: 0px 5px;
                                            text-align: center;
                                            text-decoration: none;
                                            display: inline-block;
                                            font-size: 37px;
                                            /* width: 38px; */
                                        " type="number" value="{{$item['counttovar']}}">
                <button
                    data-tovarid="{{$item['id']}}"
                    data-type="plus"
                    class="btn_cart_ajax"
                    data-tovarprice="{{$item['price']}}"
                    style="
                                            background: #b5bcce;
                                            width: 38px;
                                            border: none;
                                            color: white;
                                            padding: 0px 5px;
                                            text-align: center;
                                            text-decoration: none;
                                            display: inline-block;
                                            font-size: 37px;
                                        ">+</button>
            </div>
        </div>
        <div   style="
                                        font-size: 24px;

                                        position: relative;
                                    ">
            <img src="{{ asset('img/delete.png') }}"
                 class="delete_zero"
                 style="cursor: pointer"
                 width="32"  data-tovarid="{{$item['id']}}">
           <span  class="price_tovar"    id="itogo_{{$item['id']}}" > {{ round( ($item['counttovar']*$item['price']),4,2) }} </span>руб.
        </div>
    </div>
    @endif
@endforeach
