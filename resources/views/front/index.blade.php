@extends('front.app')

@section('content')
    <div class="section">
        <div class="intro">
            <div class="intro__fon">


                <iframe width="560" height="315"
                        style="
                            width: 100%;
                            height: 500px;
                        "
                        src="https://www.youtube.com/embed/RfN4Aq5E6co?si=2CjmsAN89rNzgf2m" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                <div class="intro__about">
                    <div class="intro__about__item">
                        <img src="img/car.png" class="intro_img"><br>

                        <button class="head__d_logodiv_btn_com1 button">Обратная связь</button>
                    </div>
                    <div class="intro__about__item">
                        <img src="img/mastera.png" class="intro_img"><br>
                        Изготовление мебели <br>
                        точно в срок
                    </div>
                    <div class="intro__about__item">
                        <img src="img/materal.png" class="intro_img"><br>
                        Современные материалы
                    </div>
                </div>
            </div>
        </div>
        <div class="intro__line">
        </div>
        <div class="intro_r">
            <div class="intro__event">Новость<br>Акция</div>
            <div class="intro__r_line">
                {!!   \App\Services\InfoblocService::getHtml('o_nas') !!}
            </div>
        </div>
    </div>

    <div class="section">
        <div class="gallery">
            <div class="container">
                <div class="gallery__inner">
                    <div class="gallery__title">Галерея</div>
                    <div class="gallery__list">


                        <?php
                            $datas=\App\Services\OblojkaService::listMain();
                        ?>
                        @foreach($datas as $item)
                            <div class="gallery__list__item"
                                 onclick="swa('{{$item->namefile}}')"
                                 style="background-image: url('{{ asset('oblojka/'.$item->namefile) }}'); background-size: cover;">
                                <div class="gallery__list__inner"  style="background-color: rgb(206, 165, 14); cursor: pointer;">
                                    <div class="gallery__list__text">
                                        <div class="gallery__list__title">{{$item->name}}</div>
                                        <div class="gallery__list__subtitle">{!!  $item->about!!}</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach






                    </div>
                    <div class="gallery__background"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
