<?php
    $activUser=0;
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        $activUser=1;
    }

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Оформление заказа</title>
    <style>
        @media screen and (max-width: 768px) {
            .card-body {
                width: 100%;
            }
            .cardzakaz1{
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="background-color: #dfe1f0">

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: white">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="/img/logo.png" height="57px">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link active" aria-current="page" href="{{ url('/') }}">Главная страница</a>
            </div>
        </div>
    </div>
</nav>


<div class="row" style="padding: 10px">
    <div class="col-6 col-md-6 cardzakaz1">
        <div class="card" style="width:100%;">
            <div class="card-body">
                <h2 class="card-title">Оформление заказа</h2>
                <form method="post" action="{{ url('/shag2zaivja') }} ">
                    @csrf
                    <h4>Способ оплаты*:</h4>
                    <div class="form-group">
                        <select class="form-control" id="id_oplata" name="oplata" required>
                            <option value="">Выберите способ оплаты</option>
                            <option value="1">Оплата картой</option>
                            <option value="2">Наличными в офисе магазина</option>
                            <option value="3">Наложенный платеж - Почта</option>
                        </select>
                    </div>
                    <h4>Доставка*:</h4>
                    <div class="form-group">
                        <label for="id-method_dostavki">Способ доставки:*</label>
                        <select class="form-control" name="method_dostavki" id="id-method_dostavki" required>
                            <option value="">Выберите способ доставки</option>
                            <option value="1">Самовывоз</option>
                            <option value="2">Доставка</option>
                            <option value="3">Почта</option>
                        </select>
                    </div>
                    <div class="form-group" id="form_adres_dost" style="display: none">
                        <label for="id_adress_dostavki">Адресс доставки*</label>
                        <input type="text" class="form-control"
                               name="adress_dostavki"
                               id="id_adress_dostavki" placeholder="Адресс">
                    </div>
                    <h4>Информация о получателе:</h4>
                    <div class="form-group">
                        <label for="id_name">Ф.И.О.*</label>
                        <input type="text" class="form-control" id="id_name"
                               @if($activUser)
                                value="{{$user->name }}"
                               @endif
                               name="fio" required placeholder="Ивано Иван Иванович">
                    </div>
                    <div class="form-group">
                        <label for="id_phone">Телефон*</label>
                        <input type="text" class="form-control" id="id_phone"
                               @if($activUser)
                                value="{{$user->phone }}"
                               @endif
                               name="phone" required placeholder="779 0 11 53">
                    </div>
                    <div class="form-group">
                        <label for="id_email">email</label>
                        <input type="email" class="form-control"
                               @if($activUser)
                               value="{{$user->email }}"
                               @endif
                               id="id_name"   name="email"  placeholder="ivan@mail.ru">
                    </div>
                    <h4>Комментарий:</h4>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <br>
                    <button type="submit">Оформление</button>
                    @csrf
                </form>
            </div>
        </div>
    </div>


    <div class="col-6 col-md-6 cardzakaz1">
        <div class="card" style="width:100%;">
            <?php
                $datas=session()->get('cardbuy');
                $sum=0;
                foreach ($datas as $data){
                    $sum+=round( ($data['counttovar']*$data['price']),4,2) ;
                }
            ?>
            <div class="card-body">
                <h2 class="card-title">Ваш заказ</h2>

            </div>
            @foreach($datas as $data)
                @if($data['counttovar']>0)
                    <div class="row" style="padding: 10px">
                        <div class="col-md-2">
                            <img src="{{$data['image']}}" style="width: 100%">
                        </div>
                        <div class="col-md-10">
                            <b>{{$data['name']}}</b><br>
                            Колличество-<b>{{$data['counttovar']}}</b> / Цена-<b>{{$data['price']}} руб.</b> /  К оплате - <b> {{ round( ($data['counttovar']*$data['price']),4,2) }} руб.</b>
                        </div>
                    </div>
                    <hr>
                @endif
            @endforeach
                <h5>К оплате за заказ без учета доставки - {{$sum}} руб.пмр</h5>
        </div>

    </div>

</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script>
        $( "#id-method_dostavki" ).change(function() {
            let val=$(this).val();
            if(val==2){
                $('#form_adres_dost').show();
                $('#id_adress_dostavki').attr('required', '');
            }else {
                $('#form_adres_dost').hide();
                $('#id_adress_dostavki').removeAttribute("required");
            }
        });
    </script>

</body>
</html>
