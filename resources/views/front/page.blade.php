@extends('front.app')

@section('content')
    <div class="catalog">
        <div class="container">
            <div class="catalog__inner news__list">
                <div class="catalog__title"> {{ $data->name }}</div>
                <div class="news__detail">
                    {!!$data->html_value  !!}
                </div>
                <div class="catalog__background"></div>
            </div>
        </div>
    </div>
@endsection
