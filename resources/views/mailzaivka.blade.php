
<h1>Заявка c сайта</h1>
<hr>
Ф.И.О - <b>{{$req['h_name']}}</b> <br>
Телефон - <b>{{$req['h_phone']}}</b> <br>
email- <b>{{$req['email']}}</b> <br>
Метод оплаты -

@if($req['h_oplata']==1)
    <b>
        Оплата картой
    </b>
@endif
@if($req['h_oplata']==3)
    <b>
        Наложенный платеж - Почта
    </b>
@endif
@if($req['h_oplata']==2)
    <b>
        Наличными в офисе магазина
    </b>
@endif<br>
Доставка -

@if($req['h_method_dostavki']==1)
    <b>
        Самовывоз
    </b>
@endif
@if($req['h_method_dostavki']==3)
    <b>
        Почта
    </b>
@endif
@if($req['h_method_dostavki']==2)
    <b>
        Доставка
    </b>
   (
        {{$req['h_adress_dostavki']}}
   )<br>

@endif
<hr>
<table>
    <tr>
        <td>
            id
        </td>
        <td>
            img
        </td>
        <td>
            название
        </td>
        <td>
           кол-во
        </td>
        <td>
           цена
        </td>
        <td>
          итого
        </td>
    </tr>
    <?php $sum=0; ?>
    @foreach($datas as $item)
        @if($item['counttovar']>0)
            <tr>

                <td>
                    {{$item['id']}}
                </td>
                <td>
                    <img src="{{$item['image']}}" width="25">

                </td>
                <td>
                    {{$item['name']}}
                </td>
                <td>
                    {{$item['counttovar']}}
                </td>
                <td>
                    {{$item['price']}}
                </td>
                <td>
                    {{$item['counttovar']*$item['price']}}
                    <?php
                        $sum=$sum+$item['counttovar']*$item['price'];
                    ?>
                </td>
            </tr>
        @endif
    @endforeach
</table>
<hr>
<h3>ИТОГО  - {{$sum}} </h3>

<p>Sending Mail from LivShop bitmebel.com</p>
