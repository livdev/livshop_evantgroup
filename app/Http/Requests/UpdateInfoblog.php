<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInfoblog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|max:110|unique:infoblocks,slug,'.$this->id,
            'html_value' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'A slug is required',
            'slug.unique' => 'Slug unique',
            'html_value.required' => 'A html text is required',
        ];
    }
}
