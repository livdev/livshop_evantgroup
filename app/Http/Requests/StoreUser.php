<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users|max:255',
            'name' => 'required',
            'password' => 'required|min:8|max:20',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'A email is required',
            'email.unique' => 'Email unique',
            'name.required' => 'A name is required',
            'password.required' => 'A password is required',
            'password.min' => 'A password is min 8',
        ];
    }


}
