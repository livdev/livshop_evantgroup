<?php

namespace App\Http\Controllers;

use App\Models\Galler;
use App\Models\GallerItem;
use Illuminate\Http\Request;

class GallerController extends Controller
{

    /**
     * Список категорий
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $datas=Galler::all();
        return view('apanel.gallary.index', ['datas'=>$datas]);
    }


    /**
     * Админка элементов
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexitem($id)
    {
        $dataGalary=Galler::all();
        $dataSelectGallary=Galler::find($id);
        $datas=GallerItem::where('gallar_id',$id)->get();
        return view('apanel.gallary.items', ['dataGalary'=>$dataGalary,
            'dataSelectGallary'=>$dataSelectGallary,
            'datas'=>$datas]);
    }

    /**
     * Покозать фото по категории поьзователя
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showgalaryitem($id){
        $dataSelectGallary=Galler::find($id);
        $datas=GallerItem::where('gallar_id',$id)->get();
        return view('front.gallaryitems', [
            'dataSelectGallary'=>$dataSelectGallary,
            'datas'=>$datas]);
    }


    /**
     * Открыть форму для создани категории
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('apanel.gallary.create');
    }

    /***
     * Добавление категории
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model=new Galler();
        $model->name=$request->input('name');
        if($request->file('namefile')){
            //   File::put("files/abc.txt", "This is content in txt file");
            $file = $request->file('namefile');
            $destinationPath = 'gallary';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            $model->namefile=$fileName;
        }
        $model->save();
        return redirect()->route('apanel.galary.index')->with('status',__('apanel.response.galary.create') );
    }

    /**
     * Показать категории
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showgalary()
    {
        $datas=Galler::all();
        return view('front.gallary', ['datas'=>$datas]);
    }

    /**
     * форма добавление новой фотографии
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createitem() {
        $datas=Galler::all();
        return view('apanel.gallary.createitem', ['datas'=>$datas]);
    }

    /**
     * Создать фото в галлерери
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function galaryitem(Request $request) {
        $model=new GallerItem();
        $model->name=$request->input('name');
        $model->gallar_id=$request->input('gallar_id');
        if($request->file('namefile')){
            //   File::put("files/abc.txt", "This is content in txt file");
            $file = $request->file('namefile');
            $destinationPath = 'gall_item';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            $model->file=$fileName;
        }
        $model->save();
        return redirect()->route('apanel.galary.index')->with('status',__('Фото добавил в категорию') );
    }

    /**
     * Редактировать галерею
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)  {
        $data=Galler::find($id);
        if(is_null($data))
            abort(404);
        return view('apanel.gallary.edit', ['data'=>$data]);
    }

    /**
     * Обновить галерею
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)  {
        $model=Galler::find($request->input('id'));
        $model->name=$request->input('name');
        if($request->file('namefile')){
            //   File::put("files/abc.txt", "This is content in txt file");
            $file = $request->file('namefile');
            $destinationPath = 'gallary';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            $model->namefile=$fileName;
        }
        $model->save();
        return redirect()->route('apanel.galary.index')->with('status',__('apanel.response.update') );
    }

    /**
     * Удалить галерею
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id){
        Galler::destroy($id);
        return redirect()->route('apanel.galary.index')->with('status',__('apanel.response.delete') );
    }


    /**
     * Форма редактирование элемент
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function itemsedit($id){
        $data=GallerItem::find($id);
        $dataGall=Galler::all();
        $seleGall=Galler::find($id);
        if(is_null($data))
            abort(404);
        return view('apanel.gallary.editeitem', ['data'=>$data,
            'seleGall'=>$seleGall,
            'dataGall'=>$dataGall]);
    }

    /**
     * Редактировать
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function itemsupdate(Request $request){
        $model=GallerItem::find($request->input('id'));
        $model->name=$request->input('name');
        $model->gallar_id=$request->input('gallar_id');
        if($request->file('namefile')){
            //   File::put("files/abc.txt", "This is content in txt file");
            $file = $request->file('namefile');
            $destinationPath = 'gall_item';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            $model->file=$fileName;
        }
        $model->save();
        return redirect()->route('apanel.galary.index')->with('status',__('apanel.response.update') );
    }

    public function itemdelete($id){
        GallerItem::destroy($id);
        return redirect()->route('apanel.galary.index')->with('status',__('apanel.response.delete') );
    }


    public function showOne($id){
        $data=GallerItem::find($id);
        if(is_null($data))
            abort(404);
        return view('front.gallaryitemsone', ['data'=>$data]);
    }

}
