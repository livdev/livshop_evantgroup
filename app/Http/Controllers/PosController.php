<?php

namespace App\Http\Controllers;

use App\Models\Pos;
use App\Models\Rasb;
use App\Models\Rash;
use App\Models\SalePos;
use App\services\AssService;
use App\services\PosService;
use Illuminate\Http\Request;

class PosController extends Controller
{

    public function index()
    {
        $datas=Pos::getPos();
        return view('upanel.pos.index',['datas'=>$datas]);
    }


    public function create()
    {
        return view('upanel.pos.create');
    }


    public function store(Request $request)
    {
        PosService::createPos($request);
        return  redirect()->route('upaenl.pos');
    }

    public function edit($id){
        $data=Pos::getPosId($id);
        $dataRass=Rash::where("pos_id",$id)->get();
        $grDataList=AssService::listGrAll();
        $saleDatas=SalePos::where('pos_id',$id)->get();
        return view('upanel.pos.edit',
            [
                'data'=>$data,
                'grDataList'=>$grDataList,
                'saleDatas'=>$saleDatas,
                'dataRass'=>$dataRass]);
    }

    public function update(Request $request){
        PosService::updatePos($request);
        return  redirect()->route('upaenl.pos');
    }

    public function destroy($id){
        PosService::destroyPos($id);
        return  redirect()->route('upaenl.pos');
    }


    public function setSale(Request $request){

        $id=$request->input('idrecord');
        $sale=$request->input('inpSale');
        $grass_id=$request->input('grSale');

        $dataGetSaleGr=SalePos::where('pos_id',$id)
            ->where('grass_id',$grass_id)
            ->first();
        if(is_null($dataGetSaleGr)){
            $dataGetSaleGr=new SalePos();
            $dataGetSaleGr->grass_id=$grass_id;
            $dataGetSaleGr->pos_id=$id;
            $dataGetSaleGr->sale=$sale;
            $dataGetSaleGr->save();
        }else{
            $dataGetSaleGr->sale=$sale;
            $dataGetSaleGr->save();
        }
        $saleDatas=SalePos::where('pos_id',$id)->get();
        $html=view('upanel.pos.sale',['saleDatas'=>$saleDatas])->render();
        return $html;
    }


    public function deleteSale(Request $request){
        $id=$request->input('idrecord');
        SalePos::where('id',$id)->delete();
        $saleDatas=SalePos::where('pos_id',$id)->get();
        $html=view('upanel.pos.sale',['saleDatas'=>$saleDatas])->render();
        return $html;
    }





}

