<?php

namespace App\Http\Controllers;

use App\Services\TelegramBot;
use Illuminate\Http\Request;
use Mail;
class FrontController extends Controller
{
    /**
     * Главная страница
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view('front.index');
    }

    /**
     * Отпровляем сообщение на почту
     *
     * @param Request $request
     * @return int
     */
    public function sendphone(Request $request){
        $data = array('name'=>$request->input('text'));

        TelegramBot::sendTo('Заявка БЕСПЛАТНЫЙ ВЫЕЗД НА ЗАМЕРЫ И СОЗДАНИЕ ПРОЕКТА-'.$request->input('text'));

        /*
        Mail::send('mail', $data, function($message) {
            $message->to('eropa@live.ru', 'клиент')->subject
            ('Заявка "ВЫЕЗД НА ЗАМЕРЫ И СОЗДАНИЕ ПРОЕКТА"');
            $message->from('evantmailryb@gmail.com','No replay');
        });
       Mail::send(['text'=>'mail'], $data, function($message) {
            $message->to('jenika06@mail.ru', 'клиент')->subject
            ('Заявка "ВЫЕЗД НА ЗАМЕРЫ И СОЗДАНИЕ ПРОЕКТА"');
            $message->from('evantmailryb@gmail.com','No replay');
        });
        */
        return 1;
    }


    public function shopitems($id){
        $datas=$this->getDataApi($id);
        $datas=json_decode($datas);
       //$datagroups=json_decode($this->getGroupList($id));
        $datagroups=null;
        $name_group="";
    /*    foreach ($datagroups as $items){
            if($items->id==$id)
                $name_group=$items->name;
        }*/
        return view('front.shopitems',['datas'=>$datas,
            'name_group'=>$name_group,
            'datagroups'=>$datagroups]);
    }

    /**
     * Получить группу элементов
     * @return bool|string
     */
    protected function getGroupList($id){
        $url = env('sklad_url')."api/getcategshop";
        $post_data = array (
            "id" => $id,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }


    protected function getTreeGroupList($id){
        $url = env('sklad_url')."api/getcategshop";
        $post_data = array (
            "id" => $id,
        );
      //  dd($id);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        //dd($output);
        curl_close($ch);
    }


    public function showTovar($id){
        $url = env('sklad_url')."api/getinfotovar";
        $post_data = array (
            "id" => $id,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        curl_close($ch);

        if(!$output)
            abort(404);

        $data=json_decode($output);


        return view('front.infotovar',['data'=>$data]);
    }

    /**
     * Получить элементы товара
     * @param $id
     * @return bool|string
     */
    protected function getDataApi($id){
        $url = env('sklad_url')."api/getshop";
        $post_data = array (
            "group_id" => $id,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
