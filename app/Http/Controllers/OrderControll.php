<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderControll extends Controller
{
    public  function  shag1(){
        return view('front.zakaz1');
    }

    public  function  shag2(Request $request){
        $data=$request->all();
        return view('front.zakaz2',['data'=>$data]);
    }

}
