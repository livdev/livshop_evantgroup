<?php

namespace App\Http\Controllers;

use App\Models\Pos;
use App\Models\Rasb;
use App\Models\Rash;
use App\Models\Status;
use App\services\getData;
use App\services\RassService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RashController extends Controller
{

    public function index()
    {
        $kassir=(Auth::user()->role=="kassir"?1:0);

        return  view('upanel.ras.index',
            [
                'kassir'=>$kassir,

            ]);
    }


    public function create()
    {
        $clients=Pos::all();

        return  view('upanel.ras.create',['clients'=>$clients]);
    }

    public function getRasass(Request $request){
        $id=$request->input('id');
        $posID=$request->input('posID');
        if($posID=="")
            $posID=0;
        $datas=getData::getAssGr($id,$posID);
        $datagr=getData::getAssGrid($id);


        return response()->json(['asstovar'=>$datas,'datagr'=>$datagr]);
    }

    public function getRasassMain(){
        $datas=getData::getAssRasNew();
        return response()->json($datas);
    }

    public function addrasxod(Request $request){
        RassService::create($request);
        return 1;
    }

    public function getToday(){
        $datas=RassService::selectToday();
        return response()->json($datas);
    }

    public function getFound(Request $request){
        $datas=getData::geRasxodFound($request);
        return response()->json($datas);
    }


    public function changestatus(Request $request){
        $a=$request->all();
        $status=Status::where('name',$request->input('status'))->first();
        if(is_null($status))
            return 1;
        $ras=Rash::find($request->input('idrecord'));
        $ras->status_id=$status->id;
        $ras->save();
        return 1;
    }


    public function getRashtml(Request $request){
       // return $request->all();
        $code_ras=$request->input('code_ras');
        $dataRasbs=Rasb::where('rash_id',$code_ras)->get();
        $html=view('upanel.pos.rasxodpos',['dataRasbs'=>$dataRasbs])->render();
        return $html;
    }

}
