<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Providers\RouteServiceProvider;
use App\Services\TelegramBot;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function createZakaz(Request $request){
        $phone=$request->input('phone');
        $name=$request->input('name');
        /*$email=$request->input('email');
        $password=$request->input('password');

        */
        $textSend ="Ф.И.О -".$request->input('h_name')."<br>";
        $textSend.="Телефон -".$request->input('h_phone')."<br>";
        $textSend.="email - ".$request->input('h_email')."<br>";
        if($request->input('h_oplata')==1)
            $textSend.="Оплата - Оплата картой<br>";
        if($request->input('h_oplata')==2)
            $textSend.="Оплата - Наличными в офисе магазина<br>";
        if($request->input('h_oplata')==3)
            $textSend.="Оплата - Наложенный платеж - Почта<br>";
        if($request->input('h_method_dostavki')==1)
            $textSend.="Метод доставки - Самовывоз<br>";
        if($request->input('h_method_dostavki')==2){
            $textSend.="Метод доставки - Доставка<br>";
            $textSend.="Адресс доставки - ".$request->input('h_adress_dostavki')."<br>";
        }
        if($request->input('h_method_dostavki')==3)
            $textSend.="Метод доставки - Почта<br>";






        $createorder=$request->input('create_order');
        $datas=null;
        if ($request->session()->has('cardbuy')) {
            $datas= session('cardbuy');
        }



       // $user=User::where('email',$email)->count();
        $user=0;
        if($user==0){
           /* DB::table('users')->insert([
                'name' =>$name,
                'email' =>$email,
                'role'=>'user',
                'phone'=>$phone,
                'password' => Hash::make($password),
            ]);
            $user=User::where('email',$email)->first();
*/

            if($createorder=='1'){
                $url = env('sklad_url')."api/setzaivka";

                $ch = curl_init();
                $post_data = array (
                    "datazaivka" => json_encode($datas),
                    "user_id"=>0,
                    "comment"=>$textSend
                );



                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                $output = curl_exec($ch);
                curl_close($ch);

               // $arrayData=array(['zakaz'=>$output,'data'=>$datas,'user_id'=>$user->id,'request'=>$request->all()]);
                $arrayData=json_encode(['zakaz'=>$output,'data'=>$datas,'user_id'=>0,'request'=>$request->all()]);
                $orderModel=new Order();
                $orderModel->data_send=json_encode($arrayData);
                $orderModel->user_id=0;
                $orderModel->comment=json_encode($request->all());
                $orderModel->save();



                TelegramBot::sendTo('Заявка c сайта');


                // $data = array('req'=>$request->all(),'datas'=>$datas);
                /*
               Mail::send('mailzaivka', $data, function($message) {
                   $message->to('eropa@live.ru', 'клиент')->subject
                   ('Заявка c сайта _'.time());
                   $message->from('evantmailryb@gmail.com','No replay');
               });

               Mail::send('mailzaivka', $data, function($message) {
                       $message->to('jenika06@mail.ru', 'клиент')->subject
                        ('Заявка c сайта _'.time());
                       $message->from('evantmailryb@gmail.com','No replay');
                   });

               if (Auth::attempt(['email' => $email, 'password' => $password])) {
               }
               */
                $request->session()->forget('cardbuy');
                return "login";
            }
            return "login";
        }
        return 'error';

    }


    public function reguser(Request $request){
        $email=$request->input('email');
        $password=$request->input('password');
        $phone=$request->input('phone');
        $name=$request->input('name');



        $user=User::where('email',$email)->count();
        if($user==0){
            DB::table('users')->insert([
                'name' =>$name,
                'email' =>$email,
                'role'=>'user',
                'phone'=>$phone,
                'password' => Hash::make($password),
            ]);
           if (Auth::attempt(['email' => $email, 'password' => $password])) {
                return redirect()->route('account.index');
            }
        }
        return  redirect()->back();

    }

}
