<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $email=$request->input('email');
        $password=$request->input('password');
        $createorder=$request->input('create_order');

        $datas=null;
        if ($request->session()->has('cardbuy')) {
            $datas= session('cardbuy');
        }

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Success

            if($createorder=='1'){
                $url = env('sklad_url')."api/setzaivka";
                $ch = curl_init();
                $post_data = array (
                    "datazaivka" => json_encode($datas),
                    "user_id"=>Auth::user()->id,
                    "comment"=>"-------"
                );
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                $output = curl_exec($ch);
                curl_close($ch);


                $arrayData=json_decode(['zakaz'=>$output,'data'=>$datas,'user_id'=>Auth::user()->id,'request'=>$request->all()]);
                $orderModel=new Order();
                $orderModel->data_send=json_encode($arrayData);
                $orderModel->user_id=Auth::user()->id;
                $orderModel->comment=json_encode($request->all());
                $orderModel->save();

                $data = array('req'=>$request->all(),'datas'=>$datas);
                /*
                Mail::send('mailzaivka', $data, function($message) {
                    $message->to('eropa@live.ru', 'клиент')->subject
                    ('Заявка c сайта _'.time());
                    $message->from('evantmailryb@gmail.com','No replay');
                });
                */
               /* Mail::send('mailzaivka', $data, function($message) {
                    $message->to('jenika06@mail.ru', 'клиент')->subject
                     ('Заявка c сайта _'.time());
                    $message->from('evantmailryb@gmail.com','No replay');
                });
               */
                $request->session()->forget('cardbuy');
                return "login";
            }else {
                return redirect()->route('account.index');
            }
        } else {
            if($createorder=='1'){
                return "error";
            }else{
                return redirect()->back();
            }

        }
    }

}
