<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInfoblog;
use App\Http\Requests\UpdateInfoblog;
use App\Models\Infoblock;
use App\User;
use Illuminate\Http\Request;

class InfoblockController extends Controller
{

    /**
     * Список всеx инфоблоков
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('isAdmin', User::class);
        $datas=Infoblock::all();
        return view('apanel.infoblock.index', ['datas'=>$datas]);
    }

    /**
     * Открываем форму на создания
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('isAdmin', User::class);
        return view('apanel.infoblock.create');

    }

    /**
     * процесс добавления новой записи
     * @param StoreInfoblog $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreInfoblog $request)
    {
        $this->authorize('isAdmin', User::class);
        $model=new Infoblock();
        $model->slug=$request->input('slug');
        $model->html_value=$request->input('html_value');
        $model->save();
        return redirect()->route('apanel.infoblock.index')->with('status',__('apanel.response.infoblock.store') );
    }

    /**
     * форма редактирования
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id){
        $this->authorize('isAdmin', User::class);
        $data=Infoblock::find($id);
        if(is_null($data))
            abort(404);
        return view('apanel.infoblock.edit', ['data'=>$data]);
    }

    /**
     * Процес обновления
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateInfoblog $request){
        $this->authorize('isAdmin', User::class);
        $id=$request->input('id');
        $model=Infoblock::find($id);
        if(is_null($model))
            abort(404);
        $model->slug=$request->input('slug');
        $model->html_value=$request->input('html_value');
        $model->save();
        return redirect()->route('apanel.infoblock.index')->with('status',__('apanel.response.infoblock.update') );
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete($id){
        $this->authorize('isAdmin', User::class);
        Infoblock::destroy($id);
        return redirect()->route('apanel.infoblock.index')->with('status',__('apanel.response.infoblock.delete') );
    }

}
