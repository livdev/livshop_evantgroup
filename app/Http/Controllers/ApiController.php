<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addtovar(Request $request){
        //  $request->session()->forget('cardbuy');
        if ($request->session()->has('cardbuy')) {
            /**
             * Если товар есть в корзине
             */
            $arraSaveSession=array();
            $iFlag=0;
            $datas= session('cardbuy');
            foreach ($datas as $data){
                if($data['id']==$request->input('tovarid')){
                    $iFlag++;
                    $arrayAdd=array('id'=>$request->input('tovarid'),
                        'name'=>$request->input('tovarname'),
                        'price'=>$request->input('price'),
                        'image'=>$request->input('image'),
                        'counttovar'=>($request->input('counttovar')+$data['counttovar']));
                }else{
                    $arrayAdd=$data;
                }
                $arraSaveSession[]=$arrayAdd;
            }
            if($iFlag==0){
                $arrayAdd=array('id'=>$request->input('tovarid'),
                    'name'=>$request->input('tovarname'),
                    'price'=>$request->input('price'),
                    'image'=>$request->input('image'),
                    'counttovar'=>$request->input('counttovar'));
                $arraSaveSession[]=$arrayAdd;
            }
            if($request->input('counttovar')>0)
                $request->session()->put('cardbuy', $arraSaveSession);
        }else {
            /**
             * Если корзина пуста то добовляем сюда
             */
            $arrayAdd=array('id'=>$request->input('tovarid'),
                'name'=>$request->input('tovarname'),
                'price'=>$request->input('price'),
                'image'=>$request->input('image'),
                'counttovar'=>$request->input('counttovar')
            );
            if($request->input('counttovar')>0)
                $request->session()->push('cardbuy', $arrayAdd);
        }
        $valSessionCart=$request->session()->get('cardbuy');
        $sum=0;
        foreach ($valSessionCart as $item){
            $sum=$sum+round( ($item['counttovar']*$item['price']),4,2);
        }
        $htmlCard=view('front.ajaxcart',['valSessionCart'=>$valSessionCart])->render();

        //return  $htmlCard;
        return  response()->json(['html'=>$htmlCard,'sum'=>$sum]);
    }

    /**
     * Изменения кол-во товара в корзине
     * @param Request $request
     */
    public function updatecount(Request $request){
        if ($request->session()->has('cardbuy')) {
            /**
             * Если товар есть в корзине
             */
            $arraSaveSession=array();
            $iFlag=0;
            $datas= session('cardbuy');
            foreach ($datas as $data){
                if($data['id']==$request->input('tovarid')){
                    $iFlag++;
                    $arrayAdd=array('id'=>$request->input('tovarid'),
                        'name'=>$request->input('tovarname'),
                        'price'=>$request->input('price'),
                        'image'=>$request->input('image'),
                        'counttovar'=>($request->input('counttovar')));
                }else{
                    $arrayAdd=$data;
                }
                $arraSaveSession[]=$arrayAdd;
            }
            if($iFlag==0){
                $arrayAdd=array('id'=>$request->input('tovarid'),
                    'name'=>$request->input('tovarname'),
                    'price'=>$request->input('price'),
                    'image'=>$request->input('image'),
                    'counttovar'=>$request->input('counttovar'));
                $arraSaveSession[]=$arrayAdd;
            }
            $request->session()->put('cardbuy', $arraSaveSession);
        }
    }

    public function countovar(Request $request){
        if ($request->session()->has('cardbuy')) {
            $count=0;
            $datas= session('cardbuy');
            foreach ($datas as $item){
                if($item['counttovar']>0)
                    $count++;
            }
            return $count;
        }
        return 0;
    }

}
