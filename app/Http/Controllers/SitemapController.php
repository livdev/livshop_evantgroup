<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index()
    {
        $pages = [
            ['url' => url('/'), 'lastmod' => now(), 'priority' => 1.0],
            ['url' => url('/galary'), 'lastmod' => now(), 'priority' => 1.0],
            ['url' => url('/page/ysluga'), 'lastmod' => now(), 'priority' => 1.0],
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sklad.evantgroup.com/api/getsitemap',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($response, TRUE);
        foreach ($json as $item){
            $pages[]=['url' => url('/tovar/'.$item['idtovar']), 'lastmod' => now(), 'priority' => 1.0];
        }

        return response()->view('sitemap.index', compact('pages'))
            ->header('Content-Type', 'text/xml');
    }
}
