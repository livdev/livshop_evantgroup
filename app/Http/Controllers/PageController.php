<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePage;
use App\Http\Requests\UpdatePage;
use App\Models\Page;
use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Главаня страница
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->authorize('isAdmin', User::class);
        $datas=Page::all();
        return view('apanel.page.index', ['datas'=>$datas]);
    }


    /**
     * Форма редактирвание
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(){
        $this->authorize('isAdmin', User::class);
        return view('apanel.page.create');
    }

    /**
     * Создание
     *
     * @param StorePage $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StorePage $request){
        $this->authorize('isAdmin', User::class);
        $model=new Page();
        $model->slug=$request->input('slug');
        $model->html_value=$request->input('html_value');
        $model->name=$request->input('name');
        $model->save();
        return redirect()->route('apanel.page.index')->with('status',__('apanel.response.page.create') );
    }

    /**
     * Форма редактирование
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id){
        $this->authorize('isAdmin', User::class);
        $data=Page::find($id);
        if(is_null($data))
            abort(404);
        return view('apanel.page.edit', ['data'=>$data]);
    }

    /**
     * Обнвление
     *
     * @param UpdatePage $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdatePage $request){
        $this->authorize('isAdmin', User::class);
        $model=Page::find($request->input('id'));
        $model->slug=$request->input('slug');
        $model->html_value=$request->input('html_value');
        $model->name=$request->input('name');
        $model->save();
        return redirect()->route('apanel.page.index')->with('status',__('apanel.response.page.update') );
    }

    public function show($slug){
        $data=Page::where('slug',$slug)->first();
        if(is_null($data))
            abort(404);
        return view('front.page', ['data'=>$data]);
    }

}
