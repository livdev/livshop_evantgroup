<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOblojka;
use App\Http\Requests\UpdateOblojka;
use App\Models\Infoblock;
use App\Models\Oblojka;
use App\User;
use Illuminate\Http\Request;

class OblojkaController extends Controller
{

    /**
     * Список всеx элеменов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('isAdmin', User::class);
        $datas=Oblojka::all();
        return view('apanel.oblojka.index', ['datas'=>$datas]);
    }

    /**
     * Форма добавление обложки
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(){
        $this->authorize('isAdmin', User::class);
        return view('apanel.oblojka.create');
    }

    /**
     * Процесс добавление новой записи
     *
     * @param StoreOblojka $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreOblojka $request){
        $this->authorize('isAdmin', User::class);
        $model=new Oblojka();
        $model->name=$request->input('name');
        $model->about=$request->input('about');
        if($request->file('namefile')){
            //   File::put("files/abc.txt", "This is content in txt file");
            $file = $request->file('namefile');
            $destinationPath = 'oblojka';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            $model->namefile=$fileName;
        }
        $model->save();
        return redirect()->route('apanel.oblojka.index')->with('status',__('apanel.response.oblojka.store') );
    }

    /**
     * Открываем форму на редактирование
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id){
        $this->authorize('isAdmin', User::class);
        $data=Oblojka::find($id);
        if(is_null($data))
            abort(404);
        return view('apanel.oblojka.edit', ['data'=>$data]);
    }

    /**
     * Процесс обновления обложки
     *
     * @param UpdateOblojka $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateOblojka $request){
        $this->authorize('isAdmin', User::class);
        $model=Oblojka::find($request->input('id'));
        if(is_null($model))
            abort(404);
        $model->name=$request->input('name');
        $model->about=$request->input('about');
        if($request->file('namefile')){
            //   File::put("files/abc.txt", "This is content in txt file");
            $file = $request->file('namefile');
            $destinationPath = 'oblojka';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            $model->namefile=$fileName;
        }
        $model->save();
        return redirect()->route('apanel.oblojka.index')->with('status',__('apanel.response.oblojka.update') );
    }


    public function delete($id){
        $this->authorize('isAdmin', User::class);
        Oblojka::destroy($id);
        return redirect()->route('apanel.oblojka.index')->with('status',__('apanel.response.delete') );
    }

}
