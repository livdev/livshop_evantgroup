<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Services\TelegramBot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
class CartController extends Controller
{
    /**
     * Покозать корзину
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function  show(Request $request){
        if ($request->session()->has('cardbuy')) {
            $valSessionCart=$request->session()->get('cardbuy');
            $sum=0;
            foreach ($valSessionCart as $item)
                $sum=$sum+$item['price']*$item['counttovar'];
            return view('front.carts', ['valSessionCart'=>$valSessionCart,'sum'=>$sum]);
        }
        return view('front.emptycart');
    }

    /**
     * Отправка заявки
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function senzaivka(Request $request){
        $datas=null;
        if ($request->session()->has('cardbuy')) {
            $datas= session('cardbuy');
        }




        dd($request->all());



        $url = env('sklad_url')."api/setzaivka";
        $ch = curl_init();
        $post_data = array (
            "datazaivka" => json_encode($datas),
            "user_id"=>1,
            "comment"=>"-------"
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        curl_close($ch);

        $data = array('req'=>$request->all(),'datas'=>$datas);
        TelegramBot::sendTo('Заявка c сайта');


        /*
        Mail::send('mailzaivka', $data, function($message) {
            $message->to('eropa@live.ru', 'клиент')->subject
            ('Заявка c сайта');
            $message->from('evantmailryb@gmail.com','No replay');
        });
        */
        /*
        Mail::send('mailzaivka', $data, function($message) {
            $message->to('jenika06@mail.ru', 'клиент')->subject
            ('Заявка c сайта');
            $message->from('evantmailryb@gmail.com','No replay');
        });
        */
        $request->session()->forget('cardbuy');
        return redirect('/');
    }


    public function ajxSend(Request $request){
        $datas=null;
        if ($request->session()->has('cardbuy')) {
            $datas= session('cardbuy');
        }

        $url = env('sklad_url')."api/setzaivka";
        $ch = curl_init();
        $post_data = array (
            "datazaivka" => json_encode($datas),
            "user_id"=>Auth::user()->id,
            "comment"=>"-------"
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        curl_close($ch);

        $arrayData=json_encode(['zakaz'=>$output,'data'=>$datas,'user_id'=>Auth::user()->id,'request'=>$request->all()]);
        $orderModel=new Order();
        $orderModel->data_send=json_encode($arrayData);
        $orderModel->user_id=Auth::user()->id;
        $orderModel->comment=json_encode($request->all());
        $orderModel->save();


        $data = array('req'=>$request->all(),'datas'=>$datas);
        TelegramBot::sendTo('Заявка c сайта*');
        /*
        Mail::send('mailzaivka', $data, function($message) {
            $message->to('eropa@live.ru', 'клиент')->subject
            ('Заявка c сайта');
            $message->from('evantmailryb@gmail.com','No replay');
        });

        */
        /*
        Mail::send('mailzaivka', $data, function($message) {
            $message->to('jenika06@mail.ru', 'клиент')->subject
            ('Заявка c сайта');
            $message->from('evantmailryb@gmail.com','No replay');
        });*/
        $request->session()->forget('cardbuy');
        return "login";
    }


    public function getstatus(){
        $user=Auth::user();
        $url = env('sklad_url')."api/getstatus";
        $dataOrders=Order::where('user_id',$user->id)->get();
        $arraId=array();
        foreach ($dataOrders as $dataOrder){
            $js_column_validation = json_decode($dataOrder->data_send);
            $js_column_validation = json_decode($js_column_validation);
            $arraId[]=$js_column_validation->zakaz ;
        }
        $ch = curl_init();
        $post_data = array (
            "orders" => json_encode($arraId),
            "user_id"=>$user->id,
            "comment"=>"-------"
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;

    }




}
