<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index(){
        $data=Auth::user();
        return view('front.account',['data'=>$data]);
    }

    public function orders(){
        $dataOrders=Order::where('user_id',Auth::user()->id)->get();
        return view('front.orders',['dataOrders'=>$dataOrders]);
    }

    public function update(Request $request){
        $data=Auth::user();
        if($request->input('type_date')==1){
            $data->name=$request->input('name');
            $data->phone=$request->input('phone');
            $data->save();
        }else{
            $data->name=Hash::make($request->input('password'));
            $data->save();
        }
        return redirect()->back();
    }
}
