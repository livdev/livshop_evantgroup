<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /**
     * Вывод список всеx пользователей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $this->authorize('isAdmin', User::class);
        $users=User::all();
        return view('apanel.user.index', ['users'=>$users]);
    }

    /*
     * Открыть форму на создание
     */
    public function create(){
        $this->authorize('isAdmin', User::class);
        return view('apanel.user.create');
    }


    /**
     * Процесс доабвление нового пользователя
     * @param StoreUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUser $request){
        $this->authorize('isAdmin', User::class);
        $user=new User();
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->password=Hash::make($request->input('password'));
        $user->role=$request->input('role');
        $user->save();
        return redirect()->route('apanel.user.index')->with('status',__('apanel.response.user.store') );
    }


    /**
     * Форма редактировния
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $this->authorize('isAdmin', User::class);
        $user=User::find($id);
        if(is_null($user))
            abort(404);
        return view('apanel.user.edit', ['user'=>$user]);
    }


    /**
     * @param UpdateUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUser $request){
        $this->authorize('isAdmin', User::class);
        $user=User::find($request->input('id'));
        $user->name=$request->input('name');
        $user->role=$request->input('role');
        $user->email=$request->input('email');
        $user->save();
        return redirect()->route('apanel.user.index')->with('status',__('apanel.response.user.update') );
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id){
        $this->authorize('isAdmin', User::class);
        User::destroy($id);
        return redirect()->route('apanel.user.index')->with('status',__('apanel.response.user.delete') );
    }
}
