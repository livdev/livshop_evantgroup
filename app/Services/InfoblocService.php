<?php


namespace App\Services;


use App\Models\Infoblock;

class InfoblocService
{
    static function getHtml($slug){
        $data=Infoblock::where('slug',$slug)->first();
        if(is_null($data))
            return "";
        return $data->html_value;
    }
}
