<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('front.home');
Route::post('/sendphone', 'FrontController@sendphone')->name('front.sendphone');
Route::get('/page/{slug}', 'PageController@show')->name('front.page');
Route::get('/galary', 'GallerController@showgalary')->name('front.showgalary');
Route::get('/galary/{id}', 'GallerController@showgalaryitem')->name('front.showgalaryitem');
Route::get('/galary/foto/{id}', 'GallerController@showOne')->name('front.showgalaryitemone');

Route::get('/shop/group/{id}','FrontController@shopitems')->name('front.shopitems');
Route::get('/carts', 'CartController@show')->name('front.cart');
Route::get('/tovar/{id}', 'FrontController@showTovar')->name('front.carttovar');

Route::post('/senzaivka', 'CartController@senzaivka')->name('front.senzaivka');
Route::get('/zaivkaok', function () {
        return view('front.zaivkaOk');
    });

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('reguser', 'Auth\RegisterController@reguser')->name('reguser');
Route::post('registerzaivka', 'Auth\RegisterController@createZakaz')->name('registerzaivka');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/shag1zaivka', 'OrderControll@shag1');
Route::post('/shag2zaivja', 'OrderControll@shag2');
Route::post('/ajxsend','CartController@ajxSend');



Route::prefix('apanel')->group(function () {
    Route::get('users', 'UserController@index')->name('apanel.user.index');
    Route::get('user/create', 'UserController@create')->name('apanel.user.create');
    Route::post('user/store', 'UserController@store')->name('apanel.user.store');
    Route::get('user/edit/{id}', 'UserController@edit')->name('apanel.user.edit');
    Route::post('user/update', 'UserController@update')->name('apanel.user.update');
    Route::get('user/delete/{id}', 'UserController@delete')->name('apanel.user.delete');

    Route::get('infoblock', 'InfoblockController@index')->name('apanel.infoblock.index');
    Route::get('infoblock/create', 'InfoblockController@create')->name('apanel.infoblock.create');
    Route::post('infoblock/store', 'InfoblockController@store')->name('apanel.infoblock.store');
    Route::get('infoblock/edit/{id}', 'InfoblockController@edit')->name('apanel.infoblock.edit');
    Route::post('infoblock/update', 'InfoblockController@update')->name('apanel.infoblock.update');
    Route::get('infoblock/delete/{id}', 'InfoblockController@delete')->name('apanel.infoblock.delete');

    Route::get('oblojkas', 'OblojkaController@index')->name('apanel.oblojka.index');
    Route::get('oblojkas/create', 'OblojkaController@create')->name('apanel.oblojka.create');
    Route::post('oblojkas/store', 'OblojkaController@store')->name('apanel.oblojka.store');
    Route::get('oblojkas/edit/{id}', 'OblojkaController@edit')->name('apanel.oblojka.edit');
    Route::post('oblojkas/update', 'OblojkaController@update')->name('apanel.oblojka.update');
    Route::get('oblojkas/delete/{id}', 'OblojkaController@delete')->name('apanel.oblojkas.delete');

    Route::get('pages', 'PageController@index')->name('apanel.page.index');
    Route::get('pages/create', 'PageController@create')->name('apanel.page.create');
    Route::post('pages/store', 'PageController@store')->name('apanel.page.store');
    Route::get('pages/edit/{id}', 'PageController@edit')->name('apanel.page.edit');
    Route::post('pages/update', 'PageController@update')->name('apanel.page.update');
    Route::get('pages/delete/{id}', 'PageController@delete')->name('apanel.page.delete');


    Route::get('galary', 'GallerController@index')->name('apanel.galary.index');
    Route::get('galary/create', 'GallerController@create')->name('apanel.galary.create');
    Route::post('galary/store', 'GallerController@store')->name('apanel.galary.store');
    Route::get('galary/createitem', 'GallerController@createitem')->name('apanel.galary.createitem');
    Route::post('galary/galaryitem', 'GallerController@galaryitem')->name('apanel.galaryitem.store');
    Route::get('galary/items/{id}', 'GallerController@indexitem')->name('apanel.galaryitem.items');
    Route::get('galary/edit/{id}', 'GallerController@edit')->name('apanel.galary.edit');
    Route::post('galary/update', 'GallerController@update')->name('apanel.galary.update');
    Route::get('galary/delete/{id}', 'GallerController@delete')->name('apanel.galary.delete');
    Route::get('galary/itemsedit/{id}', 'GallerController@itemsedit')->name('apanel.galary.itemsedit');
    Route::post('galary/itemsupdate', 'GallerController@itemsupdate')->name('apanel.galary.itemsupdate');
    Route::get('galary/itemdelete/{id}', 'GallerController@itemdelete')->name('apanel.galary.itemdelete');
});


Route::prefix('account')->group(function () {
    Route::get('/', 'AccountController@index')->name('account.index')->middleware('auth');
    Route::get('/orders', 'AccountController@orders')->name('account.orders')->middleware('auth');
    Route::post('/orders/status', 'CartController@getstatus')->name('account.getstatus')->middleware('auth');
    Route::post('/update', 'AccountController@update')->name('account.update')->middleware('auth');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/sitemap', 'SitemapController@index');